# ics-ans-role-logstash

Ansible role to install logstash.

## Role Variables

```yaml
logstash_gpg_key: https://packages.elastic.co/GPG-KEY-elasticsearch

logstash_version: 7.x

logstash_conf_template:
  - name: config
    file: iologstash.conf.j2
    dest: /etc/logstash/conf.d/iologstash.conf

logstash_repo_template:
  - name: repo
    file: logstash.repo.j2
    dest: /etc/yum.repos.d/logstash.repo

logstash_output_host: localhost:9200

logstash_filebeat_input_port: 5044

logstash_output_index_name: filebeat

...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-logstash
```

## License

BSD 2-clause
